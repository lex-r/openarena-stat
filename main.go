package main

import (
	"net"
	"fmt"
	"time"
	"log"
)

const (
	listenPort = 13777
)

var connection *net.UDPConn

func main() {

	conn, err := createConnection(listenPort)
	if err != nil {
		fmt.Errorf("Cannot create udp connection: %s", err)
	}

	connection = conn

	addr, err := searchServer()
	if err != nil {
		fmt.Printf("SearchServerError: %s", err)
	}

	fmt.Printf("Addr: %s", addr.String())
}

func createConnection(port int) (*net.UDPConn, error) {
	listenAddr, err := net.ResolveUDPAddr("udp4", fmt.Sprintf(":%d", port))
	if err != nil {
		return nil, err
	}

	conn, err := net.ListenUDP("udp4", listenAddr)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

func searchServer() (*net.UDPAddr, error) {
	port := 27960

	addr := &net.UDPAddr{
		IP:net.IPv4bcast,
		Port:port,
	}

	msg := []byte("\377\377\377\377getinfo xxx")

	for {
		for {
			// попытка отправить udp сообщение
			_, err := connection.WriteToUDP(msg, addr)
			// если при отправке возникла ошибка, то нужно подождать полминуты и попробовать снова
			if err != nil {
				log.Printf("Cannot write: %s", err)
			} else {
				break
			}
			time.Sleep(time.Second * 2)
		}

		buff := make([]byte, 1024)

		// если удалось отправить сообщение, и сервер в сети, то он должен отправить сообщение,
		// начинающееся с ����infoResponse
		for i := 0; i < 5; i++ {
			connection.SetReadDeadline(time.Now().Add(time.Second * 2))
			readBytes, udpAddr, err := connection.ReadFromUDP(buff)
			if err != nil {
				log.Printf("Cannot read from udp: %s", err)
				continue
			}

			// получено сообщение, отправленное самим же
			if string(msg) == string(buff[0:readBytes]) {
				continue
			}

			return udpAddr, nil
			//fmt.Printf("Read bytes: %d\nUDPAddr: %v\nBuff: %v\n", readBytes, udpAddr, string(buff[:readBytes]))
		}

		time.Sleep(time.Second * 2)
	}

	return nil, nil
}
