package main

import (
	"github.com/grokstat/grokstat/models"
	"github.com/grokstat/grokstat/grokstatconstants"
	"github.com/grokstat/grokstat/network"
	"github.com/imdario/mergo"
	"strings"
	"net"
	"github.com/grokstat/grokstat/protocols/helpers"
	"github.com/grokstat/grokstat/grokstaterrors"
	"github.com/grokstat/grokstat/protocols"
	"fmt"
	"time"
	"log"
	"github.com/grokstat/grokstat/bindata"
	"github.com/BurntSushi/toml"
)

type PacketErrorPair struct {
	Packet models.Packet
	Error  error
}

type ConfigFile struct {
	Protocols []protocols.ProtocolConfig `toml:"Protocols"`
}

func main() {
	//{"q3s": ["192.168.0.150"]}}
	hosts := []models.HostProtocolIdPair{
		models.HostProtocolIdPair{
			RemoteAddr:"192.168.0.150:27960",
			ProtocolId:"q3s",
		},
	}
	var configInstance ConfigFile
	debugLvl := 1
	outputLvl := 1
	messageChan := make(chan models.ConsoleMsg)

	configBinData, err := bindata.Asset("data/grokstat.toml")
	if err != nil {
		fmt.Errorf("Eror bindata asset: %s", err)
		return
	}
	toml.Decode(string(configBinData), &configInstance)

	protColl := protocols.LoadProtocolCollection(configInstance.Protocols)
	messageEndChan := make(chan struct{})

	go outputLoop(messageChan, messageEndChan, outputLvl)

	serverList, serverData, err := Query(hosts, protColl, messageChan, debugLvl)
	fmt.Printf(
		"ServerList: %v\n"+
		"ServerData: %v\n"+
		"Error: %v\n",
		serverList,
		serverData,
		err,
	)
}

func Query(hosts []models.HostProtocolIdPair, protColl models.ProtocolCollection, messageChan chan<- models.ConsoleMsg, debugLvl int) (serverHosts []string, output []models.ServerEntry, err error) {
	serverHosts = []string{}
	output = []models.ServerEntry{}

	// This is for easier server identification.
	serverProtocolMapping := models.MakeServerProtocolMapping()
	protocolMappingInChan := make(chan models.HostProtocolIdPair)

	go func() {
		for {
			mappingEntry := <-protocolMappingInChan
			serverProtocolMapping[mappingEntry.RemoteAddr] = mappingEntry.ProtocolId
		}
	}()
	//

	getProtocolOfServer := func(remoteAddr string) (string, bool) {
		protocolName, pOk := serverProtocolMapping[remoteAddr]
		return protocolName, pOk
	}

	serverEntryChan := make(chan models.ServerEntry, 9999)
	sendPacketChan := make(chan models.Packet, 9999)
	receivePacketChan := make(chan models.Packet, 9999)

	serverInitChan := make(chan struct{})
	serverStopChan := make(chan struct{})

	serverDataMap := make(map[string]models.ServerEntry)

	go func() {
		for {
			serverEntry := <-serverEntryChan
			hostname := serverEntry.Host

			oldEntry, exists := serverDataMap[hostname]
			if !exists {
				serverDataMap[hostname] = serverEntry
			} else {
				mergedEntry := oldEntry
				mergedRules := map[string]string{}

				for k, v := range mergedEntry.Rules {
					mergedRules[k] = v
				}

				mergo.Merge(&mergedEntry, serverEntry)
				mergo.Merge(&mergedRules, serverEntry.Rules)

				mergedEntry.Rules = mergedRules

				serverDataMap[hostname] = mergedEntry
			}
		}
	}()

	parseHandlerWrapper := func(packet models.Packet) (sendPackets []models.Packet) {
		sendPackets = make([]models.Packet, 0)
		var protocolName string
		protocolMappingName, pOk := getProtocolOfServer(packet.RemoteAddr)
		if pOk {
			protocolName = protocolMappingName
		} else {
			protocolIdentifiedName, iOk := identifyPacketProtocol(packet)
			if iOk {
				protocolName = protocolIdentifiedName
			}
		}
		if protocolName != "" {
			protocolEntry, protocolExists := protColl.FindById(protocolName)
			if protocolExists {
				packet.ProtocolId = protocolName
				handlerFunc := protocolEntry.Base.HandlerFunc

				if handlerFunc != nil {
					sendPackets = handlerFunc(packet, protColl, messageChan, protocolMappingInChan, serverEntryChan)
				}
			}
		}

		return sendPackets

	}

	for _, packPair := range MakePacketErrorPair(hosts, protColl) {
		packet := packPair.Packet
		err := packPair.Error

		if err == nil {
			protocolMappingInChan <- models.HostProtocolIdPair{RemoteAddr: packet.RemoteAddr, ProtocolId: packet.ProtocolId}
			sendPacketChan <- packet
		} else {
			messageChan <- models.ConsoleMsg{Type: grokstatconstants.MSG_DEBUG, Message: err.Error()}
		}
	}

	go network.AsyncNetworkServer(serverInitChan, serverStopChan, messageChan, sendPacketChan, receivePacketChan, parseHandlerWrapper, 5*time.Second)
	<-serverInitChan
	<-serverStopChan
	fmt.Printf("sf")
	for _, entry := range serverDataMap {
		serverHosts = append(serverHosts, entry.Host)
		output = append(output, entry)
	}

	return serverHosts, output, err
}


func identifyPacketProtocol(packet models.Packet) (string, bool) {
	return "STEAM", true
}

func MakePacketErrorPair(hosts []models.HostProtocolIdPair, protColl models.ProtocolCollection) (packErrPairs []PacketErrorPair) {
	packErrPairs = []PacketErrorPair{}

	for _, hostpair := range hosts {
		var hostpackets = []models.Packet{}
		var err error = nil

		hostport := strings.Split(hostpair.RemoteAddr, ":")
		protocolId := hostpair.ProtocolId
		protocol, protocolExists := protColl.FindById(protocolId)
		if protocolExists {
			host := hostport[0]
			var port string
			if len(hostport) < 2 {
				port, _ = protocol.Information["DefaultRequestPort"]
			} else {
				port = hostport[1]
			}
			ipAddr, rErr := net.ResolveIPAddr("ip4", host)
			if rErr == nil {
				addrFinal := strings.Join([]string{ipAddr.String(), port}, ":")

				reqPackets := helpers.MakeSendPackets(models.HostProtocolIdPair{RemoteAddr: addrFinal, ProtocolId: protocolId}, protColl)

				for _, reqPacket := range reqPackets {
					hostpackets = append(hostpackets, reqPacket)
				}
			} else {
				err = rErr
			}
		} else {
			err = grokstaterrors.InvalidProtocol
		}

		for _, packetFinal := range hostpackets {
			packErrPairs = append(packErrPairs, PacketErrorPair{Packet: packetFinal, Error: err})
		}
	}

	return packErrPairs
}

func outputLoop(messageChan <-chan models.ConsoleMsg, messageEndChan chan<- struct{}, outputLvl int) {
	for {
		message, mOk := <-messageChan
		if mOk {
			conditionalPrint(message, outputLvl, outputLvl >= grokstatconstants.MSG_DEBUG)
		} else {
			messageEndChan <- struct{}{}
			return
		}
	}
}

func conditionalPrint(message models.ConsoleMsg, outputLvl int, useLogging bool) {
	if message.Type <= outputLvl {
		if useLogging {
			log.Println(message.Message)
		} else {
			fmt.Println(message.Message)
		}
	}
}
